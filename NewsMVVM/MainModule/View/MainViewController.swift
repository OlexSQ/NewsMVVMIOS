//
//  ViewController.swift
//  NewsMVVM
//
//  Created by Mac on 02.06.2021.
//

import UIKit

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var mainViewModel: MainViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        callToViewModelForUIUpdate()
    }
    
    func callToViewModelForUIUpdate() {
        self.mainViewModel = MainViewModel()
        self.mainViewModel.bindMainViewModelToController = {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailsSegue", let detailVC = segue.destination as? DetaileViewController, let index = sender as? Int {
            detailVC.viewModel = self.mainViewModel.detailesViewModel(index: index)
            print("pass data with success")
        }
        
    }

    //MARK: - TableView methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.mainViewModel.newsData?.articles.count ?? 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "newsCell") as! NewsTableViewCell
        cell.titleLable.text = mainViewModel.newsData?.articles[indexPath.row].title
        cell.descriptionLabel.text = mainViewModel.newsData?.articles[indexPath.row].articleDescription
        cell.authorLabel.text = mainViewModel.newsData?.articles[indexPath.row].author.rawValue
        cell.imageURL = URL(string: mainViewModel.newsData?.articles[indexPath.row].urlToImage ?? "")
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "detailsSegue", sender: indexPath.row)
    }

}

